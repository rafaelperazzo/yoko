FROM python:3.9
USER root
WORKDIR /yoko_app
COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
RUN apt-get update
RUN apt-get install -y g++ build-essential libmariadbclient-dev libmariadb-dev-compat libmariadb-dev wkhtmltopdf 
EXPOSE 80
CMD waitress-serve --trusted-proxy '*' --trusted-proxy-headers 'x-forwarded-for x-forwarded-proto x-forwarded-port' --url-prefix=/yoko --port=80 yoko_app:app
#https://docs.pylonsproject.org/projects/waitress/en/stable/usage.html
