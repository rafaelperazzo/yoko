from flask_uploads import UploadSet
from flask_mail import Mail
from flask_wtf.csrf import CSRFProtect
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_principal import Principal,Permission,RoleNeed

'''
Objetos
'''
upload_csv = UploadSet(name='csv',extensions=('csv'))
mail = Mail()
bootstrap = Bootstrap()
csrf = CSRFProtect()
login_manager = LoginManager()
principal = Principal()

'''
Permissões
'''
permissao_admin = Permission(RoleNeed('admin'))
permissao_user = Permission(RoleNeed('user'))

'''
Configurações do pdfkit
'''
options = {
        'page-size': 'A4',
        'orientation': 'portrait',
        'margin-top': '2mm',
        'margin-right': '0mm',
        'margin-bottom': '0mm',
        'margin-left': '7mm',
        'encoding': "UTF-8",
        'quiet': '',
        'custom-header' : [
            ('Accept-Encoding', 'gzip')
        ],    
        'no-outline': None
    }

