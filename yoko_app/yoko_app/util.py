import pandas as pd
import requests
from flask import flash,redirect,url_for
from flask_mail import Message,BadHeaderError
from yoko_app import app
from yoko_app.objetos import mail
import threading
from yoko_app.database import db
import datetime
from cryptography.fernet import Fernet
import hashlib
import string
import random
from yoko_app.models.Usuarios import UserLog
import datetime

def cadastrar_log(usuario,tipo,ip='127.0.0.1'):
    if (app.config['TESTING']==False):
        novoLog = UserLog(user_id = usuario.id,tipo=tipo,ip=ip)
        db.session.add(novoLog)
        db.session.commit()

def genToken(size=24, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    """Gera um token aleatório

        Parameters
        ----------
        size : int
            Tamanho do token

        Returns
        -------
        token aleatório de tamanho size
    """
    return ''.join(random.choice(chars) for _ in range(size))

def link2csv(url,DIR,nomearquivo):
    """Converte um link de uma planilha do google drive para um CSV

        Parameters
        ----------
        url : str
            Link da planilha
        DIR : str
            pasta destino
        
        nomearquivo : str
            nome do arquivo csv

        Returns
        -------
            nome do arquivo CSV
    """
    #Baixando arquivo
    arquivo = DIR + nomearquivo
    arquivo_temp = DIR + 'temp.ods'
    try:
        r = requests.get(url, allow_redirects=True,timeout=15)
        open(arquivo_temp, 'wb').write(r.content)
        #Importando em DF
        df = pd.read_excel(arquivo_temp, engine='odf',index_col=0)
        df.to_csv(arquivo,sep=';')
    except Exception as e:
        flash("Erro ao importar do google drive: " + str(e),'erro')
        return("")
    return arquivo

def threadEnviarEmail(msg):
    """Thread que envia um e-mail

        Parameters
        ----------
        msg : str
            Mensagem a ser enviada
    """
    with app.app_context():
        if app.config['TESTING']==False:
            mail.send(msg)

def enviarEmail(html,to,subject):
    """Envia um e-mail

        Parameters
        ----------
        html : str
            HTML do template
        to : str
            destinatário da mensagem
        
        subject : str
            assunto da mensagem

        Returns
        -------
            página raiz da aplicação
    """
    para = [to]
    reply_to = "NAO-RESPONDA@ufca.edu.br"
    msg = Message(html=html,reply_to=reply_to,recipients=para,subject=subject)
    try:
        t1 = threading.Thread(target=threadEnviarEmail,args=(msg,))
        t1.start()
    except (BadHeaderError,Exception) as e:
        flash('Erro: ' + str(e),'erro')
        app.logger.debug(str(e))

    return(redirect(url_for('root')))

def comitar(mensagem):
    """Comita alterações no banco de dados

        Parameters
        ----------
        mensagem : str
            Mensagem de sucesso a ser mostrada

    """
    try:
        db.session.commit()
        flash(mensagem,'sucesso')
    except Exception as e:
        flash(str(e),'erro')
        app.logger.error(str(e))
        flash(str(e),'erro')

def data_por_extenso():
    Meses=('janeiro','fevereiro',u'março','abril','maio','junho',
       'julho','agosto','setembro','outubro','novembro','dezembro')
    agora = datetime.date.today()
    dia = agora.day
    mes=(agora.month-1)
    mesExtenso = Meses[mes]
    ano = agora.year
    resultado = str(dia) + " de " + mesExtenso + " de " + str(ano) + "."
    return resultado

class Anonimizador:
    
    """
    Uma classe para representar um objeto anonimizador

    Vários métodos para anonimizar e/ou mascarar um texto específico ou uma coluna de 
    um dataframe

    Atributos
    ----------
    Sem atributos

    Métodos
    -------
    anonymize(token)
        Encripta um texto com uma chave aleatória para posteriormente passar um hash sha1
        Sem possibilidade de volta

    
    Exemplo de uso
    --------------
        ==========================================================
        anon = Anonimizador()
        print(anon.hashBlake("RAFAEL PERAZZO"))

        df = pd.DataFrame([{'a':'a','b':'b'}, {'a':'a','b':'c'}])

        #Aplicando uma função a uma coluna do df
        df['a'] = df['a'].map(lambda x: anon.anonymize(str(x)))
        #df['b'] = df['b'].mask(df['b']!='','XXXXXXXXXXXX')
        df['b'] = anon.mask(df['b'],'XXXXXXXXX')
        #Selecionando a coluna 0
        print(df.iloc[:,[0]])
        print(df)
        ==========================================================

    """

    def __init__(self,chave) -> None:
        self.chave = chave

    def anonymize(self,token):
        """Anonimiza o texto passado como parâmetro

        Parameters
        ----------
        token : str, obrigatório
            O texto a ser anonimizado
        """
        key = Fernet.generate_key()
        f = Fernet(key)
        encriptado = f.encrypt(str.encode(token))
        texto = hashlib.sha1(encriptado).hexdigest()
        return(texto)
    
    def encrypt(self,texto):
        """Encripta o texto passado como parâmetro

        Parameters
        ----------
        chave: str, chave de criptografia

        texto : str, obrigatório
            O texto a ser encriptado
        
        Returns
        -------
        texto encriptado com a chave
           texto encriptado
        """
        f = Fernet(self.chave)
        if type(texto) is str:
            encriptado = f.encrypt(str.encode(texto))
        else:
            encriptado = f.encrypt(texto)
        return(encriptado.decode())

    def decrypt(self,texto):
        f = Fernet(self.chave)
        if type(texto) is str:
            decriptado = f.decrypt(str.encode(texto))
        else:
            decriptado = f.decrypt(texto)
        return(decriptado.decode('utf-8'))

    def genToken(self,texto):
        """Gera um hash sha1 do texto

        Parameters
        ----------
        
        texto : str, obrigatório
            O texto a ter seu hash
        
        Returns
        -------
        hash sha1 do texto
        """
        token = texto
        token = str.encode(token)
        texto_hash = hashlib.sha1(token).hexdigest()
        return(texto_hash)

    def hashSha1(self,token):
        """Passa um hash de sha1 no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash

        Returns
        -------
        sha1 do token
            assinatura sha1 do token
        """
        texto = hashlib.sha1(token.encode('utf8')).hexdigest()
        return(texto)
    
    def hashMd5(self,token):
        """Passa um hash de MD5 no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash

        Returns
        -------
        md5 do token
            assinatura sha1 do token
        """
        texto = hashlib.sha1(token.encode('utf8')).hexdigest()
        return(texto)

    def hashBlake(self,token,tamanho=24):
        """Passa um hash de Blake no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash
        
        tamanho : int , opcional

        Returns
        -------
        black do token
            assinatura blake do token
        """
        texto = hashlib.blake2b(token.encode('utf8'),digest_size=tamanho).hexdigest()
        return(texto)
    
    def mask_str(self,token,caractere,inicio,fim):
        """
        Mascara um texto qualquer

        Parameters
        ----------
        token: str
            O texto que será mascarado

        caractere : str 
            O caractere que será utilizado como máscara
        
        inicio: int
            Início de onde o texto será mascarado
        
        fim: int 
            Fim de onde o texto será mascarado

        Returns
        -------
        texto mascarado
            
        """
        if (inicio>=0) and (inicio<len(token)) and (fim>=0) and (fim<len(token)) and (fim>=inicio):
            prefixo = token[inicio:fim]
            tamanho = len(token)-len(prefixo)
            masked = prefixo + tamanho*caractere
            return masked
        else:
            return (len(token)*"*")
            
        

    def mask(self,series,text):
        """Mascara uma coluna de um dataframe pandas

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada
        
        texto : str 
            O texto que será utilizado como máscara

        Returns
        -------
        coluna do dataframe mascarada
            
        """
        return(series.mask(series!='',text))

    def maskCPF(self,series):
        """Mascara uma coluna de um dataframe pandas que represente um CPF

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (CPF)

        Returns
        -------
        coluna do CPF mascarada com o texto: XXX.XXX.XXX-XX
            
        """
        return(series.mask(series!='','XXX.XXX.XXX-XX'))
    
    def maskSIAPE(self,series):
        """Mascara uma coluna de um dataframe pandas que represente um SIAPE

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (SIAPE)

        Returns
        -------
        coluna do SIAPE mascarada com o texto: XXXXXXX
            
        """
        return(series.mask(series!='','XXXXXXX'))
    
    def maskNome(self,series):
        
        """Mascara uma coluna de um dataframe pandas que represente um Nome

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (Nome)

        Returns
        -------
        coluna do Nome mascarada com o texto: NOME DA PESSOA - PROTEGIDO
            
        """

        return(series.mask(series!='','NOME DA PESSOA - PROTEGIDO'))
