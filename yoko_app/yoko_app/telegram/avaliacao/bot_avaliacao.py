'''
Rotinas de Manipulação do BOT Telegram para o módulo de avaliação institucional
'''

from flask import render_template
from flask.helpers import url_for
import telebot
from telebot import apihelper
apihelper.ENABLE_MIDDLEWARE = True
import threading
from yoko_app import app
from yoko_app.models.sigaa import Avaliacao
import requests
from datetime import datetime
from yoko_app.models import AutenticacaoTelegram
from yoko_app.models import Usuarios
from yoko_app.database import db
import yoko_app.criptografia as criptografia
from yoko_app.objetos import permissao_admin,options
import pdfkit

aes = criptografia.crypto
bot = telebot.TeleBot(app.config['TELEGRAM_TOKEN'],parse_mode="HTML")

def iniciar_bot_avaliacao():
    """Inicia o bot de avaliação institucional, caso esteja definido no config.ini

    """
    iniciar = 0
    try:
        iniciar = int(app.config['INICIAR_TELEGRAM'])
    except:
        app.logger.error("Erro na configuração iniciar_telegram. Verificar config.ini")
        iniciar = 0
    if (iniciar==1):
        bot.polling()

def iniciar_thread_bot_avaliacao():
    t1 = threading.Thread(target=iniciar_bot_avaliacao,name="Iniciar BOT Avaliacao")
    t1.start()

@bot.middleware_handler(update_types=['message'])
def modify_message(bot_instance, message):
    """
    Manipulador de recebimento de token
    """
    if ('/start' in message.text):
        comando = str(message.text).split()
        if len(comando)==2:
            if (comando[0]=='/start'):
                message.text = comando[1]

@bot.message_handler(commands=['iniciar','start','ajuda'])
def boas_vindas_avaliacao(message):
    """Manipulador de boas vindas do bot
        Verifica se o usuário está autenticado antes de dar as boas vindas. 
        Parameters
        ----------
        commands : list
            Comando manipulados pela função: iniciar
    """
    with app.app_context():
        hora = int(datetime.now().strftime('%H'))
        autenticacao = AutenticacaoTelegram.AutenticacaoTelegram
        consulta = autenticacao.query.filter(autenticacao.chat_id==message.chat.id,autenticacao.autenticado==True).first()
        if (consulta is None): #Não está autenticado!
            try:
                mensagem = render_template('telegram/avaliacao/telegram.avaliacao.autenticacao.html')
                bot.send_message(message.chat.id,mensagem)
            except Exception as e:
                app.logger.error(str(e))
        else: #Já está autenticado
            try:
                mensagem = render_template('telegram/avaliacao/telegram.avaliacao.boasvindas.html',hora=hora,nome="")
                bot.send_message(message.chat.id, mensagem)
            except Exception as e:
                app.logger.error(str(e))

@bot.message_handler(commands=['sair','logout'])
def logout_avaliacao(message):
    """Manipulador logout do sistema
        Faz o logout, o que exige uma nova mensagem com o TOKEN para reutilizar as funcionalidades
        Parameters
        ----------
        commands : list
            Comando manipulados pela função: sair ou logout
    """
    with app.app_context():
        autenticacao = AutenticacaoTelegram.AutenticacaoTelegram
        consulta = autenticacao.query.filter(autenticacao.chat_id==message.chat.id).first()
        consulta.autenticado=False
        db.session.commit()
        bot.send_message(message.chat.id,u"Saída do sistema realizada com sucesso. Para reutilizar as funcionalidades será necessário digitar o TOKEN novamente.")

@bot.message_handler(regexp="^[A-Z]{16}$")
def autenticacao_telegram_avaliacao(message):
    """Verifica se o token pertence a algum usuário.

        Parameters
        ----------
        regexp : str
            Expressão regular para o formato TOKEN TELEGRAM (string com apenas letras maiusculas, tamanho 8)
    """
    with app.app_context():
        usuarios = Usuarios.User
        autenticacao = AutenticacaoTelegram.AutenticacaoTelegram
        consulta = usuarios.query.filter(usuarios.token==message.text).first()
        if not(consulta is None):
            siape = consulta.username
            nome = consulta.name
            bot.send_message(message.chat.id,"Bem vindo " + nome)
            consulta = autenticacao.query.filter(autenticacao.chat_id==message.chat.id).first()
            if (consulta is None): #Não está registrado como autenticado. Inserir na tabela
                novoRegistro = AutenticacaoTelegram.AutenticacaoTelegram(chat_id=message.chat.id,siape=siape,autenticado=True)
                try:
                    db.session.add(novoRegistro)
                    db.session.commit()
                    bot.send_message(message.chat.id,u"Autenticação realizada com sucesso!")
                    hora = int(datetime.now().strftime('%H'))
                    mensagem = render_template('telegram/avaliacao/telegram.avaliacao.boasvindas.html',hora=hora,nome=nome)
                    bot.send_message(message.chat.id,mensagem)
                except Exception as e:
                    app.logger.error(str(e))
                    bot.send_message(message.chat.id,str(e))
            else: #Atualizar na tabela o campo autenticado
                consulta.autenticado=True
                db.session.commit()
                hora = int(datetime.now().strftime('%H'))
                mensagem = render_template('telegram/avaliacao/telegram.avaliacao.boasvindas.html',hora=hora,nome=nome)
                bot.send_message(message.chat.id,mensagem)
        else:
            bot.send_message(message.chat.id,"Não encontrei um usuário com esse token!")

@bot.message_handler(regexp="^[0-9]{4}[.][2|1]$")
def consultar_avaliacao(message):
    """Envia a delcaração ou sumário de avaliação, caso o usuário envie mensagem
    da forma ANO.PERIODO

        Parameters
        ----------
        regexp : str
            Expressão regular para o formato ANO.PERIODO
    """
    avaliacao = Avaliacao.Avaliacao
    with app.app_context():
        try:
            '''
            Buscando o siape a partir do chat_id
            '''
            autenticacao = AutenticacaoTelegram.AutenticacaoTelegram
            consulta = autenticacao.query.filter(autenticacao.chat_id==message.chat.id,autenticacao.autenticado==True).first()
            if not(consulta is None):
                siape = str(consulta.siape)
                ano,periodo = message.text.split(sep='.')
                siape = aes.encrypt(siape)
                avaliacoes = avaliacao.query.filter(avaliacao.media_geral.is_not(None),avaliacao.siape==siape,avaliacao.ano==ano,avaliacao.periodo==periodo).first()
                declaracoes = avaliacao.query.filter(avaliacao.media_geral.is_(None),avaliacao.siape==siape,avaliacao.ano==ano,avaliacao.periodo==periodo).first()
                link_declaracoes = "LINK"
                link_avaliacoes = "LINK"
                cont = 0
                if not(avaliacoes is None):
                    token = avaliacoes.token
                    token = aes.decrypt(token)
                    link_avaliacoes = url_for('avaliacao_gerar_sumario',ano=ano,periodo=periodo,token=token,_external=True)
                    #link_avaliacoes = "http://172.19.0.3/yoko/avaliacao/" + str(ano) + "/" + str(periodo) + "/" + token + "/gerar_sumario"
                    cont = cont + 1
                    bot.send_message(message.chat.id,"Estou preparando seu sumário de avaliações, por favor aguarde...")
                    baixar_e_enviar(link_avaliacoes,bot,message)
                if not(declaracoes is None):
                    token = declaracoes.token
                    token = aes.decrypt(token)
                    #link_declaracoes = "http://172.19.0.3/yoko/avaliacao/" + str(ano) + "/" + str(periodo) + "/" + token + "/gerar_declaracao"
                    link_declaracoes = url_for('avaliacao_gerar_declaracao',ano=ano,periodo=periodo,token=token,_external=True)
                    cont = cont + 1
                    bot.send_message(message.chat.id,"Estou preparando sua declaração, por favor aguarde...")
                    baixar_e_enviar(link_declaracoes,bot,message)
            else:
                bot.send_message(message.chat.id,'Você não está autenticado: Clique em /iniciar')
        except Exception as e:
            app.logger.error("Erro no bot consultar_avaliacao")
            app.logger.error(str(e))
            bot.send_message(message.chat.id,'Erro: ' + str(e))

def baixar_e_enviar(link,bot,message):
    """Baixa um PDF do link, e envia no chat ativo do bot

        Parameters
        ----------
        link : str
            Link do documento a ser baixado (PDF)
        bot : telebot
            bot ativo
        message : telebot message
            mensagem a ser respondida
    """
    try:
        html = requests.get(link,timeout=10)
        '''
        Convertendo o HTML para PDF
        '''
        pdfkit.from_string(html.text,app.config['PDF_DIR'] + 'documento.pdf',options=options)
        arquivo = open(app.config['PDF_DIR'] + 'documento.pdf','rb')
        bot.send_document(message.chat.id,arquivo)
        bot.send_message(message.chat.id,"Obrigado por utilizar nosso serviço de autoatendimento.")
        arquivo.close()
    except Exception as e:
        app.logger.error(str(e))
        bot.send_message(message.chat.id,u"Não foi possível gerar o documento! Tente o link abaixo!")
        bot.send_message(message.chat.id,link)

iniciar_thread_bot_avaliacao()