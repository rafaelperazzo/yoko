'''

 Plataforma Yoko
    Copyright (C) 2021  Rafael Perazzo Barbosa Mota

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''
from datetime import timedelta
from flask import Flask
import os
import logging
from flask_mail import Mail
from flask import session,request,render_template
from flask_uploads import UploadSet,configure_uploads
import hashlib
import configparser
import semantic_version
import string

from . import objetos

app = Flask(__name__)

bootstrap = objetos.bootstrap
bootstrap.init_app(app)
csrf = objetos.csrf
csrf.init_app(app)
app.config['WTF_CSRF_SSL_STRICT'] = False
login_manager = objetos.login_manager
login_manager.init_app(app)
fprincipal = objetos.principal
fprincipal.init_app(app)

config = configparser.ConfigParser()
config.read('/yoko_app/yoko_app/databases/config.ini')

from git import Repo

repo = Repo('/git')
tagref = str(repo.tags[len(repo.tags)-1])

VERSION = semantic_version.Version(tagref)
app.config['APP_VERSION'] = VERSION

from . import database
db = database.db
migrate = database.migrate
migrate.init_app(app,db)

#CONFIGURAÇÕES PADRÃO
WORKING_DIR= config['DEFAULT']['working_dir']
app.config['WORKING_DIR'] = WORKING_DIR
PDF_DIR = config['DEFAULT']['pdf_dir']
app.config['PDF_DIR'] = PDF_DIR
PNG_DIR = config['DEFAULT']['png_dir']
app.config['PNG_DIR'] = PNG_DIR
IMPORT_PATH = config['DEFAULT']['working_dir'] + 'imports/'
app.config['IMPORT_PATH'] = IMPORT_PATH
HOSTNAME = config['DEFAULT']['host']
APP_MODULO = config['DEFAULT']['app_modulo']
app.config['APP_MODULO'] = APP_MODULO
APP_SISTEMA = config['DEFAULT']['app_sistema']
app.config['APP_SISTEMA'] = APP_SISTEMA
app.config['HOSTNAME'] = HOSTNAME
app.config['TELEGRAM_TOKEN'] = config['DEFAULT']['telegram_token'] 
app.config['INICIAR_TELEGRAM'] = config['DEFAULT']['iniciar_telegram']

if (config['DEFAULT']['testing']=='0'):
    app.config['TESTING'] = False
else:
    app.config['TESTING'] = True

if (config['DEFAULT']['server_name']!='localhost'):
    app.config['SERVER_NAME'] = config['DEFAULT']['server_name']

app.config["APPLICATION_ROOT"] = config['DEFAULT']['application_root']
app.config['RECAPTCHA_PUBLIC_KEY'] = config['DEFAULT']['recaptcha']
app.config['RECAPTCHA_PRIVATE_KEY'] = config['DEFAULT']['recaptcha_private']
app.config['REINICIAR'] = int(config['DEFAULT']['reiniciar'])

#E-MAIL
mail = objetos.mail
mail.init_app(app)
app.config['MAIL_SERVER'] = config['EMAIL']['mail_server']
app.config['MAIL_PORT'] = config['EMAIL']['mail_port']
app.config['MAIL_USERNAME'] = config['EMAIL']['mail_username']
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEFAULT_SENDER'] = config['EMAIL']['mail_username']
app.config['MAIL_PASSWORD'] = config['EMAIL']['mail_password']
mail = Mail(app)

#LOG
logging.basicConfig(filename=WORKING_DIR + config['DEFAULT']['log_file'], filemode=config['DEFAULT']['log_write'], format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=int(config['DEFAULT']['log_type']))

#BANCO DE DADOS
SQLITE_DIR = config['DB']['sqlite_dir']
DB_USER = config['DB']['db_user']
DB_DATABASE = config['DB']['db_database']
DB_DATABASE_TEST = config['DB']['db_database_test']
DB_PASSWORD = config['DB']['db_password']
DB_HOST = config['DB']['db_host']
if (app.config['TESTING']):
    #app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + '/' + DB_DATABASE_TEST + '?charset=utf8mb4'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + config['DB']['sqlite_dir'] + 'testing.sqlite3'
else:
    #app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST + '/' + DB_DATABASE + '?charset=utf8mb4'    
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + config['DB']['sqlite_dir'] + 'production.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = os.urandom(24)
SQLALCHEMY_BINDS = {
    'sigaa':      'sqlite:////' + SQLITE_DIR + 'sigaa.sqlite3'
}
app.config['SQLALCHEMY_BINDS'] = SQLALCHEMY_BINDS
db.init_app(app)

#TEMA
app.config['BOOTSTRAP_BOOTSWATCH_THEME'] = config['DEFAULT']['theme']

#UPLOADS
app.config['UPLOADS_DEFAULT_DEST'] = WORKING_DIR + 'upload'
app.config['UPLOADS_AUTOSERVE'] = False
upload_csv = objetos.upload_csv
upload_csv = UploadSet(name='csv',extensions=('csv'))
configure_uploads(app,(upload_csv))

#TEMPLATES
app.config['TEMPLATES_AUTO_RELOAD'] = True

#DADOS INICIAIS DE USUÁRIO
try:
    app.config['NOME_ADMINISTRADOR'] = config['DEFAULT']['nome_administrador']
    app.config['EMAIL_ADMINISTRADOR'] = config['DEFAULT']['email_administrador']
    app.config['USERNAME_ADMINISTRADOR'] = config['DEFAULT']['username_administrador']
    app.config['CPF_ADMINISTRADOR'] = config['DEFAULT']['cpf_administrador']
    app.config['SENHA_ADMINISTRADOR'] = config['DEFAULT']['senha_administrador']
except:
    app.config['NOME_ADMINISTRADOR'] = "Administrador do sistema"
    app.config['EMAIL_ADMINISTRADOR'] = "seuemail@email.com"
    app.config['USERNAME_ADMINISTRADOR'] = "admin"
    app.config['CPF_ADMINISTRADOR'] = "00000000000"
    app.config['SENHA_ADMINISTRADOR'] = "autoridade"

app.debug = False

@app.before_request
def carregar_dados_sistema():
    if (session.get('modulo')==None):
        session['modulo'] = APP_MODULO
        session['username'] = 'admin'
        session['versao'] = str(VERSION)

from yoko_app.models import Usuarios,Roles,UsuarioRole
from yoko_app.util import genToken,enviarEmail,data_por_extenso


login_manager.login_view = "/login"
login_manager.login_message = u"Autenticação necessária para o recurso selecionado"
login_manager.login_message_category = "info"

@login_manager.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (username) user to retrieve

    """
    return Usuarios.User.query.filter_by(username=user_id).first()

from flask_login import current_user
from flask_principal import identity_loaded,RoleNeed,UserNeed

@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of roles, update the
    # identity with the roles that the user provides
    if hasattr(current_user, 'roles'):
        for usuarioRole in current_user.roles:
            identity.provides.add(RoleNeed(usuarioRole.role.name))

def load_key():
    """Carrega a chave privada das bases Sqlite
    """
    try:
        key_file = open(SQLITE_DIR + 'secret.key','rb').read()
        app.config['SQLITE_KEY'] = key_file
        iv_file = open(SQLITE_DIR + 'secret.iv','rb').read()
        app.config['SQLITE_IV'] = iv_file
    except:
        app.logger.error(u'ARQUIVO DE CHAVE SECRETA NÃO ENCONTRADA')

load_key()

from yoko_app.util import Anonimizador
import yoko_app.criptografia as criptografia
crypto = Anonimizador(app.config['SQLITE_KEY'])
aes = criptografia.crypto

@app.before_first_request
def inicializar_bd():
    """Inicializa o banco de dados com um usuário admin padrão, de acordo com o definido em config.ini.
    """
    session.permanent = True
    session.permanent_session_lifetime = timedelta(minutes=20)
    if (app.config['REINICIAR']==1):
        db.drop_all(bind=None)
        db.create_all(bind=None)
    
    if (app.config['TESTING']):
        db.drop_all(bind=None)
        db.create_all(bind=None)
    
    if (len(Usuarios.User.query.all())==0):
        #Criando usuário admin
        senha_admin = str(app.config['SENHA_ADMINISTRADOR'])
        token = genToken(16,chars=string.ascii_uppercase)
        admin = Usuarios.User(name=app.config['NOME_ADMINISTRADOR'],email=app.config['EMAIL_ADMINISTRADOR'], password=hashlib.sha1(senha_admin.encode('utf8')).hexdigest(),username=app.config['USERNAME_ADMINISTRADOR'],token=token,cpf=aes.encrypt(app.config['CPF_ADMINISTRADOR']))
        db.session.add(admin)
        db.session.commit()
        adminRole = Roles.Role(name="admin",description="Administrador")
        userRole = Roles.Role(name="user",description=u"Usuário Comum")
        docenteRole = Roles.Role(name="docente",description="Docente")
        db.session.add(adminRole)
        db.session.add(userRole)
        db.session.add(docenteRole)
        db.session.commit()
        adminAdmin = UsuarioRole.UsuarioRole(user_id=admin.id,role_id=adminRole.id)
        userAdmin = UsuarioRole.UsuarioRole(user_id=admin.id,role_id=userRole.id)
        docenteAdmin = UsuarioRole.UsuarioRole(user_id=admin.id,role_id=docenteRole.id)
        db.session.add(adminAdmin)
        db.session.add(userAdmin)
        db.session.add(docenteAdmin)
        db.session.commit()

from yoko_app.views import lattes,mensagens,principal,usuarios,avaliacao,admin

from yoko_app.telegram.avaliacao import bot_avaliacao

from werkzeug.exceptions import HTTPException

def processarErro(error):
    if request.url_rule is None:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>"""
    else:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>
        <p>""" + str(request.url_rule) + """</p>"""
    mensagem_email = render_template('template_email.html',titulo=u'[ERRO - PLATAFORMA YOKO] - Notificação',mensagem=texto_email)
    enviarEmail(mensagem_email,"cimai.proplan@ufca.edu.br",u'[ERRO - PLATAFORMA YOKO] - Notificação')
    return(render_template('error.html',error=error,titulo='Erro: ' + str(error.code)))

@app.errorhandler(403)
def handle_403(error):
    if request.url_rule is None:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>"""
    else:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>
        <p>""" + str(request.url_rule) + """</p>"""
    mensagem_email = render_template('template_email.html',titulo=u'[ERRO - PLATAFORMA YOKO] - Notificação',mensagem=texto_email)
    enviarEmail(mensagem_email,"cimai.proplan@ufca.edu.br",u'[ERRO - PLATAFORMA YOKO] - Notificação')
    return(render_template('error.html',error=error,titulo='Erro: ' + str(error.code)))

@app.errorhandler(404)
def handle_404(error):
    return(render_template('error.html',error=error,titulo='Erro: ' + str(error.code)))

@app.errorhandler(500)
def handle_500(error):
    if request.url_rule is None:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>"""
    else:
        texto_email = """<p>O Erro """ + str(error.code) + """(""" + str(error) + """) ocorreu na Plataforma Yoko.</p>
        <p>""" + data_por_extenso() + """</p>
        <p>""" + str(request.url_rule) + """</p>"""
    mensagem_email = render_template('template_email.html',titulo=u'[ERRO - PLATAFORMA YOKO] - Notificação',mensagem=texto_email)
    enviarEmail(mensagem_email,"cimai.proplan@ufca.edu.br",u'[ERRO - PLATAFORMA YOKO] - Notificação')
    return(render_template('error.html',error=error,titulo='Erro: ' + str(error.code)))