from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo
from wtforms.widgets.core import Input
import re

def validar_username(form,field):
        if len(field.data)<8:
            raise ValidationError('Erro!')

def validar_token(form,field):
    if any(char.isdigit() for char in field.data):
        raise ValidationError(u"O token deve ter pelo menos 16 caracteres, com apenas letras maiúsculas!")

def password_check(form,field):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        16 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """
    password = field.data
    # calculating the length
    length_error = len(password) < 16

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None

    # overall result
    password_ok = not ( length_error or digit_error or uppercase_error or lowercase_error or symbol_error )

    if (not password_ok):
        raise ValidationError(u"A senha deve conter pelo menos 16 caracteres, sendo pelo menos 1 dígito, 1 caractere especial, maiúsculas e minúsculas.")

class UserForm(FlaskForm):
    name = StringField('Nome: ', validators=[DataRequired()])
    username = StringField('Nome de usuário: ',validators=[InputRequired()])
    email = EmailField('E-mail: ', validators=[DataRequired(),Email(check_deliverability=True)])
    token = StringField('Token Telegram',validators=[Length(min=16,max=16,message="O token deve ter 16 caracteres"),InputRequired(),validar_token])
    password = PasswordField('Password: ', validators=[InputRequired(),EqualTo('confirmar',message='As senhas devem ser iguais!'),Length(min=16,message='A senha deve ser de pelo menos 16 caracteres'),password_check])
    confirmar = PasswordField('Confirmar password: ')
    submit = SubmitField('Enviar')

    