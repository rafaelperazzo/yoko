from flask_wtf import FlaskForm,RecaptchaField
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,InputRequired,EqualTo,Length

class EsqueciMinhaSenhaForm(FlaskForm):
    email = EmailField('E-mail: ', validators=[DataRequired(),Email(check_deliverability=True)])
    recaptcha = RecaptchaField()
    submit = SubmitField('Enviar')

class RedefinirSenhaForm(FlaskForm):
    password = PasswordField('Password: ', validators=[InputRequired(),EqualTo('confirmar',message='As senhas devem ser iguais!'),Length(min=8,message='A senha deve ser de pelo menos 8 caracteres')])
    confirmar = PasswordField('Confirmar password: ')
    recaptcha = RecaptchaField()
    submit = SubmitField('Enviar')