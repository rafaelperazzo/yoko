from flask_wtf import FlaskForm, RecaptchaField
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import InputRequired
from yoko_app import app

class LoginForm(FlaskForm):
    username = StringField('SIAPE: ',validators=[InputRequired()])
    password = PasswordField('Senha: ', validators=[InputRequired()])
    if (app.config['TESTING']==False):
        recaptcha = RecaptchaField()
    submit = SubmitField('Enviar')
