from flask_wtf import FlaskForm
from wtforms import *
from wtforms.fields.html5 import *
from wtforms.validators import DataRequired,Email,Length,ValidationError,InputRequired,EqualTo
from wtforms.widgets.core import Input

class MensagemForm(FlaskForm):
    titulo = StringField('Titulo: ', validators=[DataRequired()])
    texto = TextAreaField('Texto da mensagem: ', validators=[DataRequired()])
    expiracao = DateField('Data de expiração da mensagem: ',validators=[InputRequired()])
    submit = SubmitField('Enviar')