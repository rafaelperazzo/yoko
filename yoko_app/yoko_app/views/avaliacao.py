from yoko_app import app
from flask import render_template
from flask import url_for,send_from_directory,redirect,flash
from yoko_app.database import db
from yoko_app.objetos import permissao_admin,options
from flask_login import login_required,current_user
from yoko_app.util import data_por_extenso,comitar,enviarEmail,Anonimizador
from yoko_app.models.sigaa import Avaliacao
import pdfkit
from sqlalchemy import and_
import yoko_app.criptografia as criptografia
import pyqrcode
import png
import os

avaliacao = Avaliacao.Avaliacao
periodo_letivo = Avaliacao.Periodo_letivo
crypto = Anonimizador(app.config['SQLITE_KEY'])
aes = criptografia.crypto

def decriptar(dados):
    """Decripta os dados da Avaliação dos docentes

        Parameters
        ----------
        dados : list
            Lista de objetos Avaliacao Encriptados
        
        Returns
        -------
            Lista de Objetos Avaliacao Decriptados
    """
    for dado in dados:
        dado.nome = aes.decrypt(dado.nome)
        dado.siape = aes.decrypt(dado.siape)
        dado.cpf = aes.decrypt(dado.cpf)
        dado.codigo = aes.decrypt(dado.codigo)
        dado.disciplina = aes.decrypt(dado.disciplina)
        dado.token = aes.decrypt(dado.token)
    return (dados)

@app.route('/avaliacao/listar/<tipo>')
@login_required
#@permissao_admin.require(http_exception=403)
def avaliacao_listar(tipo):
    """Lista as avaliações disponíveis

        Parameters
        ----------
        tipo : int
            0 lista os resultados e 1 lista as declarações
        
        Returns
        -------
            Lista de avaliações disponíveis
    """
    siape = current_user.get_id()
    siape = aes.encrypt(siape)
    cpf = current_user.cpf
    app.logger.debug(cpf)
    if (int(tipo)==1): #Declarações para disciplinas sem avaliação
        dados = avaliacao.query.filter(avaliacao.media_geral.is_(None),avaliacao.cpf==cpf).group_by(avaliacao.ano,avaliacao.periodo).order_by(avaliacao.ano.desc(),avaliacao.periodo.desc()).all()        
    else: #Sumário de avaliação
        dados = avaliacao.query.filter(avaliacao.media_geral.is_not(None),avaliacao.cpf==cpf).group_by(avaliacao.ano,avaliacao.periodo).order_by(avaliacao.ano.desc(),avaliacao.periodo.desc()).all()        
    dados = decriptar(dados)
    return(render_template('avaliacao/avaliacoes.html',data=dados,tipo=int(tipo),titulo=u"Documentos de autoavaliação institucional"))

@app.route('/avaliacao/<ano>/<periodo>/<token>/gerar_declaracao')
def avaliacao_gerar_declaracao(ano,periodo,token):
    """Gera a declaração para o período especificado

        Parameters
        ----------
        ano : int
            ano das disciplinas
        
        periodo : int
            periodo das disciplinas (1 ou 2)

        token: str
            token de acesso
        
        Returns
        -------
            Declaração em PDF do ano.período selecionado
    """
    ano_periodo = periodo_letivo.query.first()
    etoken = aes.encrypt(token)
    dados = avaliacao.query.filter_by(token=etoken).first()
    nome = aes.decrypt(dados.nome)
    siape = aes.decrypt(dados.siape)
    dados = avaliacao.query.filter(avaliacao.ano==ano,avaliacao.periodo==periodo,avaliacao.token==etoken,avaliacao.media_geral.is_(None)).all()
    dados = decriptar(dados)
    disciplinas = ''
    if (len(dados)>0):
        
        for linha in dados:
            disciplinas = disciplinas + linha.codigo + ' - ' + linha.disciplina + ', '
        disciplinas = disciplinas[:-1]
        if ((int(ano)==ano_periodo.ano) and (int(periodo)==ano_periodo.periodo)) or (int(ano)>ano_periodo.ano) or ((int(ano)==ano_periodo.ano) and (int(periodo)>ano_periodo.periodo)):
            motivo = u"o calendário de avaliação institucional, para o período supracitado, ainda não estar encerrado na presente data"         
        elif (int(ano)<2018):
            motivo = u'a indisponibilidade do SIGAA/UFCA em anos anteriores a 2018'
        else:
            motivo = u'quantidade inferior a cinco avaliações dos discentes para a(s) disciplina(s) supracitada(s)'
        local_data = 'Juazeiro do Norte, ' + data_por_extenso()
        qrcode_url = url_for('avaliacao_gerar_declaracao',ano=ano,periodo=periodo,token=token,_external=True)
        qrcode = pyqrcode.create(qrcode_url)
        qrcode.png(app.config['PNG_DIR'] + 'qrcode.png',scale=3)
        declaracao = render_template('avaliacao/declaracao_cimai.html',nome=nome,siape=siape,ano=ano,periodo=periodo,disciplinas=disciplinas,motivo=motivo,local_data=local_data,token=token)
        if current_user.is_authenticated:
            pdfkit.from_string(declaracao,app.config['PDF_DIR'] + 'documento.pdf',options=options)
            if (os.path.isfile(app.config['PNG_DIR'] + 'qrcode.png')):
                os.remove(app.config['PNG_DIR'] + 'qrcode.png')
            return(send_from_directory(app.config['PDF_DIR'],'documento.pdf'))
        else:
            return (declaracao)
    else:
        flash(u"Não existem disciplinas sem avaliação institucional neste período!",'erro')
        return(redirect(url_for('avaliacao_listar',siape=siape,tipo=1)))

@app.route('/avaliacao/<ano>/<periodo>/<token>/gerar_sumario')
def avaliacao_gerar_sumario(ano,periodo,token):
    """Gera o sumário de avaliação para o período especificado

        Parameters
        ----------
        ano : int
            ano das disciplinas
        
        periodo : int
            periodo das disciplinas (1 ou 2)

        token: str
            token de acesso
        
        Returns
        -------
            Sumário de Avaliação em PDF do ano/período selecionado
    """
    etoken = aes.encrypt(token)
    dados = avaliacao.query.filter_by(token=etoken).first()
    docente = aes.decrypt(dados.nome)
    dados = avaliacao.query.filter(avaliacao.media_geral.is_not(None),avaliacao.ano==ano,avaliacao.periodo==periodo,avaliacao.token==etoken).order_by(avaliacao.disciplina.desc()).all()        
    dados = decriptar(dados)
    if (len(dados)>0):
        local_data = 'Juazeiro do Norte, ' + data_por_extenso()
        qrcode_url = url_for('avaliacao_gerar_sumario',ano=ano,periodo=periodo,token=token,_external=True)
        qrcode = pyqrcode.create(qrcode_url)
        qrcode.png(app.config['PNG_DIR'] + 'qrcode.png',scale=3)
        sumario = render_template('avaliacao/sumario_avaliacao.html',componentes=dados,docente=docente,periodo=periodo,ano=ano,token=token,local_data=local_data)
        if current_user.is_authenticated:
            pdfkit.from_string(sumario,app.config['PDF_DIR'] + 'documento.pdf',options=options)
            if (os.path.isfile(app.config['PNG_DIR'] + 'qrcode.png')):
                os.remove(app.config['PNG_DIR'] + 'qrcode.png')
            return(send_from_directory(app.config['PDF_DIR'],'documento.pdf'))
        else:
            return(sumario)
    else:
        flash(u"Não existem avaliações para o período solicitado!",'erro')
        return(redirect(url_for('avaliacao_listar',tipo=1)))