from yoko_app import app
from flask import render_template
from flask import request,url_for,redirect,flash,session
from yoko_app.models import Mensagens
from yoko_app.database import db
from yoko_app.objetos import permissao_admin
from flask_login import login_required,current_user
from yoko_app.forms import formMensagem
import datetime

mensagens = Mensagens.Mensagens

@app.route('/mensagem/adicionar',methods=['POST','GET'])
@login_required
@permissao_admin.require(http_exception=403)
def mensagem_adicionar():
    form = formMensagem.MensagemForm()
    if form.validate_on_submit():
        titulo = request.form['titulo']
        texto = request.form['texto']
        expiracao = request.form['expiracao']
        data_convertida = datetime.datetime.strptime(expiracao,'%Y-%m-%d')
        mensagem = mensagens(texto=texto,expire_date=data_convertida,user_id=current_user.id,titulo=titulo)
        db.session.add(mensagem)
        db.session.commit()
        return(redirect(url_for('mensagem_listar')))
    return(render_template('form.html',form=form,action=url_for('mensagem_adicionar'),titulo=u'Adicionar nova mensagem'))

@app.route('/mensagem/listar')
@login_required
@permissao_admin.require(http_exception=403)
def mensagem_listar():
    dados = mensagens.query.order_by(mensagens.create_date.desc()).all()
    return(render_template('mensagens.html',data=dados,titulo="Mensagens cadastradas"))

@app.route('/mensagem/<id>/excluir')
@login_required
@permissao_admin.require(http_exception=403)
def mensagem_excluir(id):
    mensagem = mensagens.query.get(id)
    if mensagem is not None:
        db.session.delete(mensagem)
        db.session.commit()
        flash(u"Mensagem removida com sucesso!",'sucesso')
        return (redirect(url_for('mensagem_listar')))
    else:
        flash(u"Usuário inexistente!",'erro')
        return(redirect(url_for('mensagem_listar')))

@app.route('/mensagem/<id>/editar',methods=['POST','GET'])
def mensagem_editar(id):
    form = formMensagem.MensagemForm()
    mensagem = mensagens.query.get(id)
    if mensagem is not None:
        if form.validate_on_submit():
            titulo = request.form['titulo']
            texto = request.form['texto']
            expiracao = request.form['expiracao']
            mensagem.titulo = titulo
            mensagem.texto = texto
            mensagem.expiracao = expiracao
            db.session.commit()
            flash("Mensagem alterada com sucesso",'sucesso')
            return(redirect(url_for('mensagem_listar')))
        form.titulo.data = mensagem.titulo
        form.texto.data = mensagem.texto
        form.expiracao.data = mensagem.expire_date
        return(render_template('form.html',form=form,action=url_for('mensagem_editar',id=id),titulo=u'Editando mensagem'))
    flash("Mensagem inexistente",'erro')
    return(redirect(url_for('mensagem_listar')))
