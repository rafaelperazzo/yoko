from flask_login.utils import logout_user
from yoko_app import app
from flask import render_template
from flask import request,url_for,redirect,flash,current_app,session
from yoko_app.util import cadastrar_log
from yoko_app.models import Mensagens
from yoko_app.database import db
from flask_login import current_user,login_user
from flask_login import login_required
from yoko_app.forms import Login
from yoko_app.models import Usuarios,Mensagens
import hashlib
from flask_principal import Identity,AnonymousIdentity,identity_changed
import pyqrcode
import png
import os

#@login_required
#@permissao_admin.require(http_exception=403)

mensagens = Mensagens.Mensagens

@app.route('/')
def root():
    """Rota raiz do sistema.
    """
    dados = mensagens.query.order_by(mensagens.expire_date).limit(4).all()
    if current_user.is_authenticated:
        return render_template('index_auth.html',titulo='Plataforma Yoko',dados=dados)
    else:
        return render_template('index.html',titulo='Plataforma Yoko',dados=dados)

@app.route('/login', methods=["GET", "POST"])
def login():
    """Rota de autenticação de usuários. 

    Caso o usuário não esteja autenticado, um formulário é aberto. Caso contrário, o usuário
    é direcionado para a página inicial.

    """
    if not current_user.is_authenticated:
        form = Login.LoginForm()
        if form.validate_on_submit():
            usuario = request.form['username']
            senha = request.form['password']
            senha = hashlib.sha1(senha.encode('utf8')).hexdigest()
            if app.config['TESTING']==False:
                resultado = Usuarios.User.query.filter_by(username=usuario,password=senha).first()
            else:
                resultado = Usuarios.User.query.filter_by(username=usuario).first()
                
            if (resultado is not None): #Encontrado usuário
                login_user(resultado)
                identity_changed.send(current_app._get_current_object(),identity=Identity(resultado.id))
                '''
                Adicionado papeis do usuários na sessão
                '''
                userRoles = current_user.roles
                papeis = []
                for userRole in userRoles:
                    papeis.append(userRole.role.name)
                session['roles'] = papeis
                cadastrar_log(current_user,1,ip=str(request.remote_addr))
                flash("Bem vindo(a)" + resultado.name,'sucesso')
                flash("Não esqueça de deixar seu perfil sempre atualizado!",'sucesso')
                return(redirect(url_for('root')))
            else: #Senha não confere
                flash(u"Usuário/senha inválidos",'erro')
        return(render_template('form.html',form=form,action=url_for('login'),titulo="Autenticação"))
    else:
        return(render_template(redirect(url_for('root'))))

@app.route('/logout',methods=["GET"])
@login_required
def logout():
    """Rota de logout do sistema. o flask-principal é informado que o usuário saiu do sistema.
    """
    cadastrar_log(current_user,0,ip=str(request.remote_addr))
    logout_user()
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)
    identity_changed.send(current_app._get_current_object(),identity=AnonymousIdentity())
    return(redirect(url_for('root')))

@app.route('/privacidade',methods=["GET"])
def privacidade():
    return(render_template('privacidade.html'))

@app.route('/ajuda',methods=["GET"])
def ajuda():
    return(render_template('ajuda.html'))

@app.route('/token_telegram',methods=["GET"])
@login_required
def token_telegram():
    token = current_user.token
    qrcode_url = "https://t.me/CimaiUFCAbot?start=" + token
    qrcode = pyqrcode.create(qrcode_url)
    qrcode.png(app.config['PNG_DIR'] + 'telegram.png',scale=5)
    return(render_template('token_telegram.html',token=token,link=qrcode_url))