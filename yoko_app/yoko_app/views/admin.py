from yoko_app import app
from flask import url_for,redirect,flash,render_template
from yoko_app.database import db
from yoko_app.objetos import permissao_admin
from flask_login import login_required
from yoko_app.models.sigaa import Avaliacao
from yoko_app.models import Usuarios,UsuarioRole
from yoko_app.util import genToken
import string
from sqlalchemy.exc import IntegrityError as ErroSqlAlchemy
import yoko_app.criptografia as criptografia
from yoko_app.models import AutenticacaoTelegram

usuarios_sigaa = Avaliacao.Usuarios
aes = criptografia.crypto
autenticacao_telegram = AutenticacaoTelegram.AutenticacaoTelegram
logs = Usuarios.UserLog

def ajustarTexto(texto):
    novoTexto = texto.lstrip()
    novoTexto = novoTexto.rstrip()
    return novoTexto

@app.route('/admin/importarUsuarios',methods=['GET'])
@login_required
@permissao_admin.require(http_exception=403)
def importar_usuarios():
    """Importa usuários do SIGAA
    """
    consulta = usuarios_sigaa.query.all()
    cont = 0
    for usuario_sigaa in consulta:
        token = genToken(16,chars=string.ascii_uppercase)
        try:
            siape = aes.decrypt(usuario_sigaa.siape)
            consulta = Usuarios.User.query.filter(Usuarios.User.username==siape).all()
            if (len(consulta)==0):
                if (siape!="1570709")and(siape!="5560226"):
                    try:
                        password=genToken(16)
                        siape = aes.decrypt(usuario_sigaa.siape)
                        siape = ajustarTexto(siape)
                        nome = aes.decrypt(usuario_sigaa.nome)
                        nome = ajustarTexto(nome)
                        email = aes.decrypt(usuario_sigaa.email)
                        email = ajustarTexto(email)
                        if email=="nan":
                            email = siape
                        novoUsuario = Usuarios.User(name=nome,username=siape,password=password,token=token,email=email,cpf=usuario_sigaa.cpf)
                    except Exception as e:
                        flash(str(e),'erro')
                        return(redirect(url_for('root')))
                    db.session.add(novoUsuario)
                    db.session.commit()
                    novoUsuarioUser = UsuarioRole.UsuarioRole(user_id=novoUsuario.id,role_id=2) #user
                    novoUsuarioDocente = UsuarioRole.UsuarioRole(user_id=novoUsuario.id,role_id=3) #docente
                    db.session.add(novoUsuarioUser)
                    db.session.add(novoUsuarioDocente)
                    db.session.commit()
                    cont = cont + 1
        except ErroSqlAlchemy as e:
            app.logger.error("Erro para o usuario: " + usuario_sigaa.nome + " ---> " + str(e))
            flash("Erro para o usuario: " + usuario_sigaa.nome + " ---> " + str(e),'erro')
            return(redirect(url_for('root')))

    flash(str(cont) + u" usuários importados com sucesso!",'sucesso')
    return(redirect(url_for('root')))


@app.route('/admin/reiniciar_telegram',methods=['GET'])
@login_required
@permissao_admin.require(http_exception=403)
def reiniciar_telegram():
    autenticacao_telegram.query.delete()
    flash(u'Autenticação do telegram reiniciada com sucesso!','sucesso')
    return(redirect(url_for('root')))

@app.route('/admin/logs',methods=['GET'])
@login_required
@permissao_admin.require(http_exception=403)
def admin_logs():
    data = logs.query.order_by(logs.create_date.desc()).all()
    return(render_template('logs/relatorio_logs.html',data=data,titulo=u'Relatório de acessos'))