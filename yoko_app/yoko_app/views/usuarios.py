from yoko_app import app
from flask import render_template
from flask import request,url_for,redirect,flash
from yoko_app.database import db
from yoko_app.forms import formUsuario,EsqueciMinhaSenha
from yoko_app.objetos import permissao_admin
from flask_login import login_required,current_user
from yoko_app.util import comitar,enviarEmail,genToken
import hashlib
from yoko_app.models import Usuarios,UsuarioRole

@app.route('/usuario/adicionar',methods=['POST','GET'])
@login_required
@permissao_admin.require(http_exception=403)
def usuario_adicionar():
    form = formUsuario.UserForm()
    if form.validate_on_submit():
        name = request.form['name']
        username = request.form['username']
        email = request.form['email']
        token = request.form['token']
        token = token.upper()
        password = request.form['password']
        senha_sem_hash = password
        password=hashlib.sha1(password.encode('utf8')).hexdigest()
        novoUsuario = Usuarios.User(name=name,username=username,email=email,password=password,token=token)
        db.session.add(novoUsuario)
        comitar(u'Usuário adicionado com sucesso!')
        userNovoUsuario = UsuarioRole.UsuarioRole(user_id=novoUsuario.id,role_id=2)
        db.session.add(userNovoUsuario)
        comitar(u'Usuário adicionado com sucesso!')
        texto = """
            <p>Usuario: {}</p>
            <p>Senha: {}</p>
            <p>URL: {}</p>
        """
        texto_email = render_template('template_email.html',titulo=u'Criação de usuário',mensagem=texto.format(username,senha_sem_hash,url_for('root')))
        enviarEmail(texto_email,email,"[PLATAFORMA YOKO]")
        return (redirect(url_for('usuario_listar')))
    return(render_template('form.html',form=form,action=url_for('usuario_adicionar'),titulo=u'Adicionar novo usuário'))

@app.route('/usuario/listar')
@login_required
@permissao_admin.require(http_exception=403)
def usuario_listar():
    data = Usuarios.User.query.order_by(Usuarios.User.name).all()
    return(render_template('usuarios.html',titulo=u'Lista de Usuários',data=data))
    
@app.route('/usuario/<id>/excluir')
@login_required
@permissao_admin.require(http_exception=403)
def usuario_excluir(id):
    usuario = Usuarios.User.query.get(id)
    if usuario is not None:
        db.session.delete(usuario)
        comitar(u'Usuário removido com sucesso')
        return (redirect(url_for('usuario_listar')))
    else:
        flash("Usuário inexistente!",'erro')
        return(redirect(url_for('root')))

@app.route('/usuario/<id>/editar',methods=['POST','GET'])
@login_required
@permissao_admin.require(http_exception=403)
def usuario_editar(id):
    form = formUsuario.UserForm()
    usuario = Usuarios.User.query.get(id)
    if form.validate_on_submit():
        usuario.name = request.form['name']
        usuario.username = request.form['username']
        usuario.email = request.form['email']
        usuario.token = request.form['token']
        password = request.form['password']
        usuario.password = hashlib.sha1(password.encode('utf8')).hexdigest()
        comitar(u'Usuário atualizado com sucesso!')
        return (redirect(url_for('usuario_listar')))
    form.name.data = usuario.name
    form.username.data = usuario.username
    form.email.data = usuario.email
    form.token.data = usuario.token
    return(render_template('form.html',form=form,action=url_for('usuario_editar',id=id),titulo=u'Modificando usuário'))

@app.route('/usuario/<id>/senha',methods=['POST','GET'])
@login_required
def usuario_senha(id):
    id_usuario = int(id)
    usuario_logado = int(current_user.id)
    if (usuario_logado!=id_usuario):
        flash("Permissão negada: " + str(current_user.id) + " - " + str(id) ,'erro')
        return(redirect(url_for('root')))

    form = formUsuario.UserForm()
    usuario = Usuarios.User.query.get(id)
    if form.validate_on_submit():
        usuario.name = request.form['name']
        usuario.username = request.form['username']
        usuario.email = request.form['email']
        usuario.token = request.form['token']
        password = request.form['password']
        usuario.password = hashlib.sha1(password.encode('utf8')).hexdigest()
        comitar(u'Usuário atualizado com sucesso!')
        return (redirect(url_for('root')))
    form.name.data = usuario.name
    form.username.data = usuario.username
    form.email.data = usuario.email
    form.token.data = usuario.token
    return(render_template('form.html',form=form,action=url_for('usuario_editar',id=id),titulo=u'Modificando usuário'))

@app.route('/usuario/esqueci_minha_senha',methods=['POST','GET'])
def esqueci_minha_senha():
    form = EsqueciMinhaSenha.EsqueciMinhaSenhaForm()
    usuarios = Usuarios.User
    if form.validate_on_submit():
        email = request.form['email']
        consulta = usuarios.query.filter(usuarios.email==email).first()
        if (consulta is None): #Não encontrou o e-mail
            flash(u"Não existe nenhum usuário cadastrado com este e-mail!",'erro')
            flash(u"Entre em contato com cimai.proplan@ufca.edu.br para resolução do problema, informando seu e-mail, nome e siape.",'erro')
            return(redirect(url_for('esqueci_minha_senha')))
        else: #Encontrou o e-mail, gerar um token de alteração de senha
            token = genToken(28)
            novaMudancaSenha = Usuarios.MudancaSenha(user_id=consulta.id,token=token)            
            db.session.add(novaMudancaSenha)
            db.session.commit()
            mensagem = render_template('template.email.recuperar_senha.html',nome=consulta.name,token=token)
            mensagem_email = render_template('template_email.html',titulo=u"RECUPERAÇÃO DE SENHA",mensagem=mensagem)
            enviarEmail(mensagem_email,email,u'[PLATAFORMA YOKO] - REDEFINIÇÃO DE SENHA')
            flash(u"Um email foi enviado para (" + email + ") com instruções sobre como fazer a mudança de senha.",'sucesso')            
            return(redirect(url_for('root')))
    return(render_template('form.html',form=form,action=url_for('esqueci_minha_senha'),titulo=u'Recuperando a senha'))

@app.route('/usuario/redefinir_senha/<token>',methods=['POST','GET'])
def redefinir_senha(token):
    mudanca_senha = Usuarios.MudancaSenha
    consulta = mudanca_senha.query.filter(mudanca_senha.token==token).order_by(mudanca_senha.id.desc()).first()
    if (consulta is None): #Token não encontrado
        flash(u'Token inválido ou expirado!')
        return(redirect(url_for('root')))
    else: #Recuperar o user_id e abrir formulário de modificação de senha
        user_id = consulta.user_id
        form = EsqueciMinhaSenha.RedefinirSenhaForm()
        if form.validate_on_submit():
            editUser = Usuarios.User.query.get(user_id)
            password = request.form['password']
            editUser.password = hashlib.sha1(password.encode('utf8')).hexdigest()
            db.session.commit()
            flash("Senha atualizada com sucesso!",'sucesso')
            return(redirect(url_for('root')))
        return(render_template('form.html',form=form,action=url_for('redefinir_senha',token=token),titulo=u'Redefinindo senha'))
