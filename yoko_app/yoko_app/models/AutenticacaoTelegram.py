from enum import unique
from yoko_app.database import db
from sqlalchemy.sql import func

class AutenticacaoTelegram(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, unique=True,nullable=False)
    siape = db.Column(db.Integer,unique=False,nullable=False)
    autenticado = db.Column(db.Boolean,nullable=False,default=True)
    create_date = db.Column(db.DateTime,unique=False,nullable=True,default=func.now())
