from yoko_app.database import db

class Avaliacao(db.Model):
    __bind_key__='sigaa'
    __table__name = 'avaliacao'
    index = db.Column('index',db.Integer, primary_key=True)
    siape = db.Column('siape',db.String)
    cpf = db.Column('cpf_cnpj',db.String)
    nome = db.Column('docente',db.String)
    ano = db.Column('ano',db.Integer)
    periodo = db.Column('periodo',db.Integer)
    codigo = db.Column('codigo',db.String)
    disciplina = db.Column('disciplina',db.String)
    media_geral = db.Column('media_geral',db.Float)
    desvio_padrao = db.Column('desvio_padrao_geral',db.Float)
    token = db.Column('token',db.String)

class Periodo_letivo(db.Model):
    __bind_key__='sigaa'
    __table__name = 'periodo_letivo'
    index = db.Column('index',db.Integer, primary_key=True)
    ano = db.Column('ano',db.Integer)
    periodo = db.Column('periodo',db.Integer)

class Usuarios(db.Model):
    __bind_key__='sigaa'
    __table__name = 'usuarios'
    index = db.Column('index',db.Integer, primary_key=True)
    nome = db.Column('nome',db.String)
    siape = db.Column('siape',db.String)
    cpf = db.Column('cpf',db.String)
    password = db.Column('password',db.String)
    email = db.Column('email',db.String)
