from enum import unique
from sqlalchemy.orm import backref
from yoko_app.database import db
import datetime
from sqlalchemy.sql import func

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=False, nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    cpf = db.Column(db.String(200), unique=True, nullable=True)
    password = db.Column(db.String(80), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=True)
    create_date = db.Column(db.DateTime,unique=False,nullable=True,default=func.now())
    authenticated = db.Column(db.Boolean, default=False)
    token = db.Column(db.String(20),nullable=True,unique=True)
    roles = db.relationship('UsuarioRole',backref='User',lazy=True,cascade="all, delete")
    logs = db.relationship('UserLog',backref='User',lazy=True,cascade="all, delete")
    mensagens = db.relationship('Mensagens',backref='User',lazy=True,cascade="all, delete")

    def __repr__(self):
        return '<User %r>' % self.username
    
    def is_active(self):
        return True

    def get_id(self):
        return self.username

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

class MudancaSenha(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    token = db.Column(db.String(30),nullable=True,unique=True)
    create_date = db.Column(db.DateTime,unique=False,nullable=True,default=func.now())

class UserLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    create_date = db.Column(db.DateTime,unique=False,nullable=True,default=func.now())
    tipo = db.Column(db.Integer,nullable=False)
    ip = db.Column(db.String(20))
    #user = db.relationship('User',backref=db.backref('UserLog',lazy=True))