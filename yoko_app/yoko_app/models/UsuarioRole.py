from yoko_app.database import db

class UsuarioRole(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    role_id = db.Column(db.Integer,db.ForeignKey('role.id'),nullable=False)
    #user = db.relationship('User',backref=db.backref('UsuarioRole',lazy=True))
    role = db.relationship('Role',backref=db.backref('UsuarioRole',lazy=True))