from yoko_app.database import db
import datetime
from sqlalchemy.sql import func

class Mensagens(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    titulo = db.Column(db.String(50), unique=False, nullable=True)
    texto = db.Column(db.Text, unique=False)
    create_date = db.Column(db.DateTime,unique=False,nullable=True,default=func.now())
    expire_date = db.Column(db.DateTime,unique=False,nullable=True)
    #user = db.relationship('User',backref=db.backref('Mensagens',lazy=True))
