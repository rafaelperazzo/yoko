from yoko_app import app
from Crypto import Random
from Crypto.Cipher import AES
import base64
import hashlib

class AESCipher(object):

    def __init__(self, key,iv): 
        self.bs = AES.block_size
        self.key = hashlib.sha256(key.encode()).digest()
        self.iv = iv

    def encrypt(self, raw):
        raw = self._pad(raw)
        #iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CFB, self.iv)
        return (base64.b64encode(self.iv + cipher.encrypt(raw.encode()))).decode()

    def encrypt2(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        return (base64.b64encode(iv + cipher.encrypt(raw.encode()))).decode()

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

crypto = AESCipher(app.config['SQLITE_KEY'].decode(),app.config['SQLITE_IV'])
