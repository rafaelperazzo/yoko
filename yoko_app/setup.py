from setuptools import setup

setup(
    name='yoko_app',
    packages=['yoko_app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
