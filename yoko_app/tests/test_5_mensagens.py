import pytest
from test_0_inicial import client,preparacao,fix_logout,get_table
from test_1_auth import login, logout

def adicionar_mensagem(client, titulo, texto,expiracao):
    return client.post('/mensagem/adicionar', data=dict(
        titulo=titulo,
        expiracao=expiracao, texto=texto
    ), follow_redirects=True)

def editar_mensagem(client, titulo, texto,expiracao):
    return client.post('/mensagem/1/editar', data=dict(
        titulo=titulo,
        expiracao=expiracao, texto=texto
    ), follow_redirects=True)

@pytest.mark.usefixtures('preparacao')
def test_0_mensagem_listar(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/mensagem/listar',follow_redirects=True)
    assert b'Erro:' not in rv.data

@pytest.mark.usefixtures('preparacao')
def test_1_mensagem_adicionar(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/mensagem/adicionar',follow_redirects=True)
    assert b'Erro:' not in rv.data
    post = adicionar_mensagem(client,"Primeira Mensagem","Texto da Mensagem","2021-09-09")
    assert b'Erro:' not in post.data
    post = adicionar_mensagem(client,"Segunda Mensagem","Texto da Mensagem","2021-09-09")
    assert b'Erro:' not in post.data

@pytest.mark.usefixtures('preparacao')
def test_2_mensagem_listar(client):
    logout(client)
    login(client,'1570709')
    linhas = get_table(client,'/mensagem/listar')
    assert (len(linhas)>0)

@pytest.mark.usefixtures('preparacao')
def test_3_mensagem_editar(client):
    logout(client)
    login(client,'1570709')
    editar = client.get('/mensagem/1/editar',follow_redirects=True)
    assert b'Primeira Mensagem' in editar.data
    editar = editar_mensagem(client,"Primeira Mensagem Alterada","mudei o texto","2021-09-21")
    assert b'alterada com sucesso' in editar.data
    assert b'Primeira Mensagem Alterada' in editar.data
    assert b'mudei o texto' in editar.data


@pytest.mark.usefixtures('preparacao')
def test_4_mensagem_remover(client):
    logout(client)
    login(client,'1570709')
    excluir = client.get('/mensagem/1/excluir',follow_redirects=True)
    assert b'removida com sucesso' in excluir.data
    excluir = client.get('/mensagem/2/excluir',follow_redirects=True)
    assert b'removida com sucesso' in excluir.data