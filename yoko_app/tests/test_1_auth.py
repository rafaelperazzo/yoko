import pytest
from test_0_inicial import client,preparacao,fix_logout,get_random_user,get_usuarios

def login(client, username):
    return client.post('/login', data=dict(
        username=username,
        password='12345'
    ), follow_redirects=True)


def logout(client):
    return client.get('/logout', follow_redirects=True)

@pytest.mark.usefixtures('preparacao')
def test_0_login_process(client):
    logout(client)
    usuario = get_random_user().username
    rv = login(client,usuario)
    assert b'Erro:' not in rv.data
    assert b'Bem vindo' in rv.data

@pytest.mark.usefixtures('fix_logout')
def test_1_login_problem_process(client):
    logout(client)
    usuario = get_random_user().username
    rv = login(client,usuario)
    assert b'rio/senha inv' in rv.data

@pytest.mark.usefixtures('preparacao')
def test_2_logout_process(client):
    rv = logout(client)
    assert b'Erro:' not in rv.data

@pytest.mark.usefixtures('preparacao')
def test_3_token_telegram(client):
    logout(client)
    usuario = get_random_user().username
    rv = login(client,usuario)
    rv = client.get('/token_telegram')
    assert 'TOKEN DO TELEGRAM' in rv.get_data().decode('utf8').upper()

def test_4_email_usuarios():
    for linha in get_usuarios():
        if ('@ufca' not in linha.email):
            print(linha.name)

@pytest.mark.usefixtures('preparacao')
def test_5_perfil(client):
    logout(client)
    usuario = get_random_user()
    login(client,usuario.username)
    user_id = usuario.id
    perfil = client.get('/usuario/' + str(user_id) +'/senha')
    assert 'MODIFICANDO USU' in perfil.get_data().decode('utf8').upper()

@pytest.mark.usefixtures('preparacao')
def test_6_login_username(client):
    logout(client)
    usuario = get_random_user().username
    login(client,usuario)
    perfil = client.get('/')
    assert str(usuario) in perfil.get_data().decode('utf8').upper()