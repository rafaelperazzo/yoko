import pytest
from test_0_inicial import client,preparacao,fix_logout
from test_1_auth import login, logout
from yoko_app import app
from yoko_app.models import Usuarios

def adicionar_usuario(client, nome, username,email,token,password,confirmar):
    return client.post('/usuario/adicionar', data=dict(
        name=nome, username=username,email=email, token=token,password=password,confirmar=confirmar
    ), follow_redirects=True)

def editar_usuario(client, nome, username,email,token,password,confirmar):
    return client.post('/usuario/2/editar', data=dict(
        name=nome, username=username,email=email, token=token,password=password,confirmar=confirmar
    ), follow_redirects=True)

@pytest.mark.usefixtures('preparacao')
def test_0_usuario_listar(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/usuario/listar',follow_redirects=True)
    assert b'Lista de' in rv.data

@pytest.mark.usefixtures('preparacao')
def test_1_usuario_adicionar(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/usuario/adicionar',follow_redirects=True)
    assert b'Adicionar novo' in rv.data
    with app.app_context():
        consulta = Usuarios.User.query.filter(Usuarios.User.username=="0000000").first()
        if (consulta is None):
            #ADICIONAR
            post = adicionar_usuario(client,"NOME DO USUARIO","0000000","p_10@ufca.edu.br","AAAAAAAAAAAAAAAA","uh3!EUKvUTVw$tcim","uh3!EUKvUTVw$tcim")
            assert b'adicionado com sucesso' in post.data
            '''
            #EDITAR
            rv = client.get("/usuario/" + str(consulta.id) + "/editar",follow_redirects=True)
            assert b"NOME DO USUARIO" in rv.data
            assert b"p_10@ufca.edu.br" in rv.data
            assert b"AAAAAAAAAAAAAAAA" in rv.data
            #EXCLUIR
            rv = client.get("/usuario/" + str(consulta.id) + "/excluir",follow_redirects=True)
            assert b'removido com sucesso' in rv.data
            '''
        else:
            rv = client.get("/usuario/" + str(consulta.id) + "/excluir",follow_redirects=True)
            assert b'removido com sucesso' in rv.data

@pytest.mark.usefixtures('preparacao')
def test_2_usuario_editar(client):
    #EDITAR
    with app.app_context():
        consulta = Usuarios.User.query.filter(Usuarios.User.username=="0000000").first()
    if consulta is not None:
        rv = client.get("/usuario/" + str(consulta.id) + "/editar",follow_redirects=True)
        assert b"NOME DO USUARIO" in rv.data
        assert b"p_10@ufca.edu.br" in rv.data
        assert b"AAAAAAAAAAAAAAAA" in rv.data
    else:
        raise AssertionError

@pytest.mark.usefixtures('preparacao')
def test_3_usuario_excluir(client):
    #EDITAR
    with app.app_context():
        consulta = Usuarios.User.query.filter(Usuarios.User.username=="0000000").first()
    if consulta is not None:
        rv = client.get("/usuario/" + str(consulta.id) + "/excluir",follow_redirects=True)
        assert b'removido com sucesso' in rv.data
    else:
        raise AssertionError