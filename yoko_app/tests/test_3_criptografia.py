import pytest
from yoko_app import app
from yoko_app import criptografia

from yoko_app.util import Anonimizador,genToken
import yoko_app.criptografia as criptografia
crypto = Anonimizador(app.config['SQLITE_KEY'])
aes = criptografia.crypto

def test_0_encrypt_decrypt():
    texto = genToken(40)
    encriptado = aes.encrypt(texto)
    decriptado = aes.decrypt(encriptado)
    assert texto==decriptado

