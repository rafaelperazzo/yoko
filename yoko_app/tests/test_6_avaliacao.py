import pytest
from test_0_inicial import client, get_table,preparacao,fix_logout,get_usuarios,get_table
from test_1_auth import login, logout
from bs4 import BeautifulSoup

@pytest.mark.usefixtures('preparacao')
def test_0_avaliacao_declaracoes(client):
    logfile = open('tests/tests.log','w')
    logfile.write("###################################\n")
    logfile.write("USUÁRIOS COM 0 DECLARAÇÕES\n")
    logfile.write("###################################\n")
    for user in get_usuarios():
        logout(client)
        login(client,user.username)
        linhas = get_table(client,'/avaliacao/listar/1')
        if (len(linhas)==0):
            logfile.write(str(user.name) + " - " + str(user.username) + '\n')
        '''
        for linha in linhas:
            dados = linha.find_all('td')
            token = dados[0].get_text()
            ano = dados[3].get_text()
            periodo = dados[4].get_text()
            documento = client.get('/avaliacao/' + str(ano) + '/' + str(periodo) + '/' + str(token) + '/gerar_declaracao',follow_redirects=True)
            resposta = documento.content_type
            if 'html' in resposta:
                mensagem = documento.get_data().decode('utf8')
                if u"ainda não está disponível" not in mensagem:
                    print(usuario)
                    print(ano)
                    print(periodo)
                    raise AssertionError("erro")
            else:
                if 'pdf' not in documento.content_type:
                    print(usuario)
                    raise AssertionError("erro")
        '''
        
    logfile.close()
              
@pytest.mark.usefixtures('preparacao')
def test_1_avaliacao_sumarios(client):
    logfile = open('tests/tests.log','a')
    logfile.write("###################################\n")
    logfile.write("USUÁRIOS COM 0 SUMÁRIOS\n")
    logfile.write("###################################\n")
    for user in get_usuarios():
        logout(client)
        login(client,user.username)
        linhas = get_table(client,'/avaliacao/listar/0')
        if (len(linhas)==0):
            logfile.write(str(user.name) + " - " + str(user.username) + '\n')
        assert linhas is not None
        '''
        for linha in linhas:
            dados = linha.find_all('td')
            token = dados[0].get_text()
            ano = dados[3].get_text()
            periodo = dados[4].get_text()
            documento = client.get('/avaliacao/' + str(ano) + '/' + str(periodo) + '/' + str(token) + '/gerar_sumario',follow_redirects=True)
            resposta = documento.content_type
            assert 'pdf' in resposta
        '''
    logfile.close()



