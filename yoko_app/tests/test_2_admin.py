import pytest
from test_0_inicial import client,preparacao,fix_logout,get_random_user,get_usuarios
from test_1_auth import login, logout

@pytest.mark.usefixtures('preparacao')
def test_0_admin_telegram(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/admin/reiniciar_telegram',follow_redirects=True)
    logout(client)
    assert b'reiniciada com sucesso' in rv.data

@pytest.mark.usefixtures('preparacao')
def test_1_admin_logs(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/admin/logs',follow_redirects=True)
    logout(client)
    assert b'Erro:' not in rv.data

@pytest.mark.usefixtures('preparacao')
def test_2_admin_importar_usuarios(client):
    logout(client)
    login(client,'1570709')
    rv = client.get('/admin/importarUsuarios',follow_redirects=True)
    logout(client)
    assert b'importados com sucesso' in rv.data
    usuarios = get_usuarios()
    assert len(usuarios)>1


