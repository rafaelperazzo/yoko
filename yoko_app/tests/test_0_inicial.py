from yoko_app import app
import pytest
from yoko_app.models import Usuarios
import random
from bs4 import BeautifulSoup

aplicacao = app.test_client()

def get_random_user():
    with app.app_context():
        id_number = random.randint(1,300)
        total = len(Usuarios.User.query.all())
        if total<3:
            id_number=1
        random_user = Usuarios.User.query.get(id_number)
        return (random_user)

def get_usuarios():
    with app.app_context():
        usuarios = Usuarios.User.query.all()
    return (usuarios)

def get_table(client,route):
    rv = client.get(route,follow_redirects=True)
    html = rv.get_data().decode('utf8')
    soup = BeautifulSoup(html,'html.parser')
    tabela = soup.find('table')
    linhas = tabela.find('tbody').find_all('tr')
    return (linhas)


@pytest.fixture
def preparacao():
    app.config['WTF_CSRF_ENABLED'] = False
    yield
    app.config['WTF_CSRF_ENABLED'] = True

@pytest.fixture
def fix_logout():
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['TESTING'] = False
    yield
    app.config['WTF_CSRF_ENABLED'] = True
    app.config['TESTING'] = True

@pytest.fixture
def client():
    """A test client for the app."""
    return (aplicacao)

def test_0_root_view(client):
    rv = client.get('/root')
    assert b'500' not in rv.data

@pytest.mark.usefixtures('preparacao')
def test_1_ajuda_view(client):
    rv = client.get('/ajuda')
    assert b'Primeiro acesso' in rv.data

@pytest.mark.usefixtures('preparacao')
def test_2_privacidade_view(client):
    rv = client.get('/privacidade')
    assert b'Todos os dados'  in rv.data

@pytest.mark.usefixtures('preparacao')
def test_3_login_view(client):
    rv = client.get('/login')
    assert b'Senha:' in rv.data

def recuperar_senha(client, email):
    return client.post('/usuario/esqueci_minha_senha', data=dict(email=email), follow_redirects=True)

@pytest.mark.usefixtures('preparacao')
def test_4_esqueci_minha_senha_process(client):
    rv = client.get('/usuario/esqueci_minha_senha')
    assert b"Erro:" not in rv.data
    email = "rafael.mota@ufca.edu.br"
    rv = recuperar_senha(client,email)
    assert b"Erro:" not in rv.data
    assert b'Um email foi enviado para' in rv.data
    email = "inexistente-xxx@ufca.edu.br"
    rv = recuperar_senha(client,email)
    assert b"Erro:" not in rv.data
    assert b'Um email foi enviado para' not in rv.data
