# Plataforma Yoko - UFCA

**aplicação em flask com mysql, docker e docker-compose**

**Plataforma integrada de aplicativos UFCA: Extrator Lattes, Avaliação Institucional (a ser pensado)**

**Versão: 0.2.8**

## Requisitos

* [Docker](https://docs.docker.com/get-docker)
* [Docker-compose](https://docs.docker.com/compose/install/)

## Arquivo de configuração config.ini (modelo)

```
[DEFAULT]
testing = 0
working_dir = /yoko_app/yoko_app/
pdf_dir = /yoko_app/yoko_app/pdf/
png_dir = /yoko_app/yoko_app/static/png/
font_path = /fonts/Times_New_Roman_Bold.ttf
theme = lux
log_file = app.log
log_write = w
log_type = 10
host= localhost:9000
app_modulo= Principal
app_sistema = Yoko
telegram_token = 
iniciar_telegram = 0
nome_administrador = Administrador do Sistema
email_administrador = 
username_administrador = 
senha_administrador = 
server_name = localhost
application_root = /yoko 
recaptcha = 
recaptcha_private = 
reiniciar = 0

[DB]
db_user = yoko
db_database = yoko
db_host = db_yoko
db_password = 
sqlite_dir = /yoko_app/yoko_app/databases/

[EMAIL]
mail_server = smtp.gmail.com
mail_port = 465
mail_username = 
mail_password = 

```

## Deploy

```

1) docker pull origin master
2) cp docker-compose.yml.sample docker-compose.yml
3) Ajuste os mounts (./mounts)
4) cp yoko_app/yoko_app/config.ini.sample yoko_app/yoko_app/databases/
5) Ajuste o config.ini
6) Copie o sigaa.sqlite3 e as chaves de criptografia para yoko_app/yoko_app/databases/
7) Inicie o docker: docker-compose up -d
8) curl -s -o /dev/null LINK DO SISTEMA
9) ./test (testar o sistema)
10) Acesse a URL no navegador! :)
```

## Saída

* http://host:9000


## Autor

* Prof. Rafael Perazzo Barbosa Mota ( rafael.mota (at) ufca.edu.br )

## RECURSOS
- [x] Gerenciamento de usuários (criar, alterar, listar e excluir, recuperação de senha)
- [x] Gerenciamento de mensagens (criar, alterar, listar e excluir)
- [ ] Extração de informações do Currículo Lattes
- [x] Declaração de Avaliação Institucional - Para disciplinas não avaliadas
- [x] Sumário de Avaliação Institucional - Para disciplinas avaliadas
- [x] Módulo Avaliação Institucional - Bot Telegram
- [x] Logs
- [x] Testes automatizados
- [x] Autenticação por link e por QrCode

## TODO
- [ ] Gerenciamento de mensagens - Adicionar mensagens na página principal

## LINKS

* <https://www.papersizes.org/a-sizes-in-pixels.htm>
* <https://pythonhosted.org/Flask-Uploads/>
* <https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/>
* <https://github.com/kristoferjoseph/flexboxgrid>
* <http://flexboxgrid.com/>
* <https://flask.palletsprojects.com/en/2.0.x/>
* <https://www.gnu.org/licenses/licenses.html>
* <https://flask.palletsprojects.com/en/2.0.x/api/>
* <https://realpython.com/using-flask-login-for-user-management-with-flask/>
* <https://flask-login.readthedocs.io/en/latest/>
* <https://pythonhosted.org/Flask-Principal/>
* <https://docs.sqlalchemy.org/en/14/orm/tutorial.html#common-filter-operators>
* <https://docs.sqlalchemy.org/en/latest/core/sqlelement.html#sqlalchemy.sql.operators.ColumnOperators>
